// FMG Coding Challenge Frontend Client Web Application

//The following instructions apply to only the frontend application and not the API

Setup Instructions:
1. Clone respository into directory
2. In console/terminal run 'npm install' and 'bower install' to install all needed dependancies
3. Once everything has been installed run 'npm start' and the httpServer be running correctly in port 8000
4. In a browser navigate to 'http://localhost:8000/#!/'


