'use strict';

angular.module('myApp')
.component('landing', {
  templateUrl: 'landing/landing.template.html',
  controller: ["$scope", function ($scope) {
    $scope.$onInit = function () {
      //set view of landing page, switches between video, signIn and signUp
      $scope.landngView = 'video';
    };

    //switch between views
    $scope.changeView = function (view){
      $scope.landingView = view;
    };
  
  }]
})
.controller('HomeCtrl',
  ["$sce", function ($sce) {
    //video configs
    this.config = {
      sources: [
        {src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.mp4"), type: "video/mp4"},
        {src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm"},
        {src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg"}
      ],
      tracks: [
        {
          src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
          kind: "subtitles",
          srclang: "en",
          label: "English",
          default: ""
        }
      ],
      theme: "bower_components/videogular-themes-default/videogular.css",
      plugins: {
        poster: "http://www.videogular.com/assets/images/videogular.png"
      }
    };
  }]
);