'use strict';

angular.module('myApp')
.component('resetPassword', {
  templateUrl: 'resetPassword/resetPassword.template.html',
  controller: ['$scope', '$http', '$state', 'AuthService' , function($scope, $http, $state, AuthService){
    $scope.password;
    $scope.confirmPassword;
    $scope.message;
    
    $scope.init = function(){
      console.log('OnInit');
      console.log('state', $state.params)
      $scope.clientId = $state.params.clientId.toString();
      console.log('$scope.clientId', $scope.clientId)
    }

    $scope.cancel = function(){
      $state.go('home');
    }

    $scope.submit = function(){
      console.log('Submit')
      console.log($scope.password);
      if($scope.password !== $scope.confirmPassword){
        $scope.message = "Passwords do not match, please re-enter them."
      }
      else{
        $http.post('http://localhost:3000/api/client/' + $scope.clientId + '/resetPassword', {
          password: $scope.password,
        }).then(function(x){
          console.log("Success:", x);
          AuthService.signOut();
          $state.go('landing');
        }, function(y){
          console.log("Failed: ", y)
          $scope.message = x.data.message;
        });
      }
    }
  }]
});