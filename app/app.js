'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngResource',
  'ngSanitize',
  'ui.router',
  'ngFileUpload',
  "com.2fdevs.videogular",
  "com.2fdevs.videogular.plugins.controls",
  "com.2fdevs.videogular.plugins.overlayplay",
  "com.2fdevs.videogular.plugins.poster"
])
  .config(function ($stateProvider) {
    //app states
    var states = [
      {
        name: 'landing',
        url: '/landing',
        component: 'landing'
      },
      {
        name: 'home',
        url: '/',
        component: 'home'
      },
      {
        name: 'profile',
        url: '/profile',
        component: 'profile',
      },
      {
        name: 'editProfile',
        url: '/editProfile',
        component: 'editProfile',
      },
      {
        name: 'resetPassword',
        url: '/resetPassword/{clientId}',
        component: 'resetPassword',
      }

    ]

    // Loop over the state definitions and register them
    states.forEach(function (state) {
      $stateProvider.state(state);
    });
  });


