'use strict';

angular.module('myApp')
  .component('editProfile', {
    templateUrl: 'editProfile/editProfile.template.html',
    controller: ['$scope', '$http', '$state', 'AuthService', function ($scope, $http, $state, AuthService) {
      //initialising function
      $scope.init = function () {
        var id = AuthService.getId().toString(); //getting id and token
        var token = AuthService.getToken()

        //API requestion for clientInfo to populate form
        $http({
          method: 'GET', url: 'http://localhost:3000/api/client/' + id, headers: {
            'Authorization': token
          }
        }).then(function (x) {
          //success 
          $scope.edit = {         //populate form
            first_name: x.data.clientInfo.first_name,
            last_name: x.data.clientInfo.last_name,
            favourite_colour: x.data.clientInfo.favourite_colour,
          }
          $scope.message
        }, function (y) {
          // error, unhandled
          console.log("Failed: ", y)
        });
      }

      //cancel editing info -> return to home component
      $scope.cancel = function () {
        $state.go('home');
      }

      //submit updates
      $scope.submit = function () {
        $http({
          method: 'POST',
          url: 'http://localhost:3000/api/client/' + AuthService.getId() + '/edit',
          headers: { 'Authorization': AuthService.getToken() },
          data: {
            first_name: $scope.edit.first_name,
            last_name: $scope.edit.last_name,
            favourite_colour: $scope.edit.favourite_colour,
            email: $scope.edit.email
          }
        }).then(function (x) {
          //success -> navigation back to home           
          $state.go('home');
        }, function (y) {
          //error, unhandled
          console.log("Failed: ", y)
        });
      }

    }]
  });