'use strict';

angular.module('myApp')
.component('profile', {
  templateUrl: 'profile/profile.template.html',
  controller: ['$scope', '$state','$http', 'AuthService', function($scope, $state, $http, AuthService){
    $scope.editInfo = function(){
      $state.go('editProfile')
    }

    $scope.sendResetPassword = function(){
      console.log('sendResetPassword');
      $http({
        method: 'GET', 
        url: 'http://localhost:3000/api/client/' + AuthService.getId() + '/sendResetPassword',  
        headers: {'Authorization': AuthService.getToken() }
      }).then(function(x){
        $scope.message = x.data.message;
        console.log("Success:", x);
      }, function(y){
        console.log("Failed: ", y)
      });
    }

    //initialising function
    $scope.init = function(){
     
      var id = AuthService.getId().toString();  //clientId from localstorage

      //get clientInfo
      $http({
        method: 'GET', 
        url: 'http://localhost:3000/api/client/' + id, 
        headers: {'Authorization': AuthService.getToken() }
      }).then(function(x){
        //success, add info to $scope.profile
        $scope.profile = x.data.clientInfo;
        //profile pic url
        $scope.profilePicUrl = "http://localhost:3000/" + $scope.profile.profile_pic_url;
      }, function(y){
        //error, unhandled
        console.log("Failed: ", y)
      });
    }

  }]
});