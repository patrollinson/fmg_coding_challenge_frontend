'use strict';

angular.module('myApp')
  .component('signUp', {
    templateUrl: 'signup/signup.template.html',
    controller: ['$scope', '$http', '$state', 'Upload', function ($scope, $http, $state, Upload) {
      $scope.first_name;
      $scope.last_name;
      $scope.favourite_colour;
      $scope.email;
      $scope.password;
      $scope.message;

      //submit function
      $scope.submit = function () {
        if ($scope.form.file.$valid && $scope.file) {
          $scope.upload($scope.file);
        }
      };

      //upload, post request to API
      $scope.upload = function (file) {
        Upload.upload({
          url: 'http://localhost:3000/api/client/signup',
          data: {
            'first_name': $scope.first_name,
            'last_name': $scope.last_name,
            'favourite_colour': $scope.favourite_colour,
            'email': $scope.email,
            'password': $scope.password,
            'profile_pic': file,
          }
        }).then(function (resp) {
          //success
          $state.go('home');
        }, function (resp) {
          //error
          $scope.message = resp.data.error;
        }, function (evt) {
          //in progress

        });
      };
    }]
  });