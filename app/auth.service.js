//auth service, methods simply dealing with token and clientId stored in local storage in browser, 
//token used to validate client when requesting data from API
//clientId also in requesting data 

angular.module('myApp')
    .factory('AuthService', ['$rootScope', function($rootScope) {
    return {
        getToken: function() {
            return localStorage.getItem('token');
        },
        setToken: function(token) {
            localStorage.setItem('token', token);
        },
        getId: function() {
            return localStorage.getItem('id');
        },
        setId: function(id) {
            localStorage.setItem('id', id);
        },
        signOut: function() {
            console.log("Removing ID and Token");
            localStorage.removeItem('id');
            localStorage.removeItem('token');
            return true
        },
    };
}]);