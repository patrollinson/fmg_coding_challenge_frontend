'use strict';

angular.module('myApp')
.component('signIn', {
  templateUrl: 'signin/signin.template.html',
  controller: ['$scope', '$http', '$state', 'AuthService', function($scope, $http, $state, AuthService){
    $scope.email;
    $scope.password;
    $scope.message;

    $scope.submit = function(){
      console.log('Submit')
      console.log(
        $scope.email,
        $scope.password,
        $scope.message
      );

      $http.post('http://localhost:3000/api/client/signin', {
        email: $scope.email,
        password: $scope.password,
      }).then(function(x){
        if(x.data.error){
          $scope.message = x.data.error
        }
        else{
          console.log("Success:", x);
          console.log('Token: ', x.data.token);
          console.log('ID: ', x.data.client._id);
          AuthService.setToken(x.data.token);
          AuthService.setId(x.data.client._id);
          console.log(AuthService.getToken);
          console.log(AuthService.getID);
          $state.go('home');
        }
        
        
      }, function(y){
        console.log("Failed: ", y)
      });
    }

    

  }]
});