'use strict';

angular.module('myApp')
.component('home', {
  templateUrl: 'home/home.template.html',
  controller: ['$scope', '$state', '$http', 'AuthService', function($scope, $state, $http, AuthService){
    
    //initialising function
    $scope.init = function(){
      //test whether client has logged in
      var id = AuthService.getId();
      if(id !== null){
        //user has logged in
        //initilize home component var
        $scope.homeView = 'home';   
        $scope.logPage = 1;               //initial page number of activity log report
        $scope.loadLogs($scope.logPage);  //API request for first page
      }
      else{
        //not logged in, then route back to landing component
        console.log('No ID')
        $state.go('landing');
      }
    }
    
    $scope.signOut =  function(){
      //remove localstorage vars and routh to landing page
      AuthService.signOut();
      $state.go('landing');
    }

    $scope.changeHomeView= function(view){
      //change between home page views, (home, profile and edit profile pages)
      $scope.homeView = view;
    }

    //change page navigation  
    $scope.viewLogPage = function(page){
      if(page === 'prev'){
        $scope.logPage = $scope.logPage - 1; //setting requested page
      }
      else if(page === 'next'){
        $scope.logPage = $scope.logPage + 1; //setting requested page
      }
      else{ 
        $scope.logPage = page;               //setting requested page
      }
      $scope.loadLogs($scope.logPage); //request correct page

    }

    //requesting activity log page from API
    $scope.loadLogs = function(page){

      $scope.log = [];      //clear current log page
      $scope.pages = [];    //clear array of all pages
      
      $http({
        method: 'GET', 
        url: 'http://localhost:3000/api/client/' + AuthService.getId() + '/log/' + page, 
        headers: {'Authorization': AuthService.getToken() } 
      }).then(function(x){
        //success 
        $scope.logs = x.data.activityLog;     //set return array to log page
        $scope.numLogs = x.data.total;        //set total number of activity event logged
        $scope.numPages = Math.ceil( $scope.numLogs/ 5);  //calculate to number of pages need to display all logs

        for(var p = 0; p < $scope.numPages; p ++){ //population pages array
          $scope.pages.push(p + 1);
        }
      }, function(y){
        //error, unhandled
        console.log("Failed: ", y)
      });
    }

  }]
});